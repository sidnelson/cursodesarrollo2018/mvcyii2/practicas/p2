<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "articulos".
 *
 * @property int $id_ar
 * @property string $titulo_ar
 * @property string $textocorto_ar
 * @property string $textolargo_ar
 * @property string $foto_ar
 */
class Articulos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'articulos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['titulo_ar', 'textocorto_ar', 'textolargo_ar', 'foto_ar'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_ar' => 'Id',
            'titulo_ar' => 'Titulo del Articulo',
            'textocorto_ar' => 'Texto corto',
            'textolargo_ar' => 'Texto largo',
            'foto_ar' => 'Foto',
        ];
    }
}
