<?php
use yii\helpers\Html;
?>

<div class="col-sm-12 col-md-6 flex-grow">
    <div class="thumbnail">
      <div class="caption">
        <figure>
            <?=Html::img("@web/imgs/$clavemodelo->foto_no",[
                    'class'=>'img-responsive',
                    'style'=>[
                        'width'=>'550px',
                        'height'=>'250px',
                    ],
                    ]); ?>
        </figure>
        
        <h3>Esto sería el titulo: <?= $clavemodelo->titulo_no; ?></h3>
        <p>Esto sería el texto: <?= $clavemodelo->texto_no;?></p>
       
      </div>
    </div>
  </div>

