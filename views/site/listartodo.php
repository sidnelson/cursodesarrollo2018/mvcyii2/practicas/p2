<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>
<div class="noticias-index">

    <h1>Noticias</h1>

</div>

    <?= GridView::widget([
        'dataProvider' => $dataProvidern,
        'columns' => [
            'titulo_no',
            [
              'label'=>'otro',
               'value'=>function($data){
                return strlen($data->titulo_no);
              }  
            ],
            'texto_no',
            [
            'label'=>'Foto',
            'format'=>'raw',
                 'options'=>[
                    'style'=>'width:310px',
                ],
            'value' => function($data){
                return Html::img("@web/imgs/$data->foto_no",[
                    'class'=>'img-responsive',
                    'style'=>[
                        'width'=>'300px',
                        'height'=>'150px',
                    ],
                    ]); 
            }
        ],
            
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<div class="noticias-index">

    <h1>Articulos</h1>

</div>
        <?= GridView::widget([
        'dataProvider' => $dataProvidera,
        'columns' => [
            'titulo_ar',
            'textocorto_ar',
            [
            'label'=>'Foto',
            'format'=>'raw',
                'options'=>[
                    'style'=>'width:310px',
                ],
            'value' => function($data){
                return Html::img("@web/imgs/$data->foto_ar",[
                    'class'=>'img-responsive',
                    'style'=>[
                        'width'=>'300px',
                        'height'=>'150px',
                    ],
                    ]);
            }
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    
    
    
</div>

