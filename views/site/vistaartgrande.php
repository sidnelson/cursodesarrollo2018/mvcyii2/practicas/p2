<?php
use yii\helpers\Html;
?>
<div class="col-sm-12 col-md-6 flex-grow">
    <div class="thumbnail">
      <div class="caption">
       
        <h2><?= $model->titulo_ar;?></h2>
        <h3><?= $model->textolargo_ar;?></h3>
 
        <figure>
            <?= Html::img("@web/imgs/$model->foto_ar",[
                    'class'=>'img-responsive',
                    'style'=>[
                        'width'=>'400px',
                        'height'=>'200px',
                        'text-align'=>'center'
                    ],
                    ]); ?>
        </figure>
        
      </div>
    </div>
  </div>